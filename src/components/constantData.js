export const ageConstant = [
    {
        "option": "A",
        "value": 20,
        "marks": 0
    },
    {
        "option": "B",
        "value": 35,
        "marks": 0
    },
    {
        "option": "C",
        "value": 50,
        "marks": 0
    },
    {
        "option": "D",
        "value": 50,
        "marks": 0
    }
]

export const savingsConstant = [
    {
        "option": "A",
        "value": "10",
        "marks": 2.5
    },
    {
        "option": "B",
        "value": "25",
        "marks": 5
    },
    {
        "option": "C",
        "value": "40",
        "marks": 7.5
    },
    {
        "option": "D",
        "value": "41",
        "marks": 10
    }
]

export const retirementConstat = [
    {
        "option": "A",
        "value": 0,
        "marks": 2.5
    },
    {
        "option": "B",
        "value": 10,
        "marks": 5
    },
    {
        "option": "C",
        "value": 20,
        "marks": 7.5
    },
    {
        "option": "D",
        "value": 21,
        "marks": 10
    }
]

export const emergencyConstant = [
    {
        "option": "A",
        "value": 3,
        "marks": 2.5
    },
    {
        "option": "B",
        "value": 4,
        "marks": 5
    },
    {
        "option": "C",
        "value": 7,
        "marks": 7.5
    },
    {
        "option": "D",
        "value": 10,
        "marks": 10
    }
]


export const checkOption = (array, checkerValue) => {
    return array.find(item => {
        return item.value >= checkerValue;
    });
}