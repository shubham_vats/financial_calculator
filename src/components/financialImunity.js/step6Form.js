import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import '../../css/financialScore.css';

const Step6Form = ({ values, setFieldValue, errors, touched }) => {
  return (
    <div className='step2form step6form'>
      {' '}
      <div className=''>
        <div className='question'>
          In case of emergency how long your current savings will last?
        </div>
        <div className='answers'>
          <div
            className={
              values.emergency === 3 ? 'answerDiv selected' : 'answerDiv'
            }
            onClick={() => {
              setFieldValue('emergency', 3);
            }}
          >
            3 months
          </div>
          <div
            className={
              values.emergency === 4 ? 'answerDiv selected' : 'answerDiv'
            }
            onClick={() => {
              setFieldValue('emergency', 4);
            }}
          >
            4-6 months
          </div>
          <div
            className={
              values.emergency === 7 ? 'answerDiv selected' : 'answerDiv'
            }
            onClick={() => {
              setFieldValue('emergency', 7);
            }}
          >
            7-9 months
          </div>
          <div
            className={
              values.emergency === 9 ? 'answerDiv selected ' : 'answerDiv black'
            }
            onClick={() => {
              setFieldValue('emergency', 9);
            }}
          >
            {' '}
            {'>'} 9 months
          </div>
        </div>
        {errors.emergency && touched.emergency && (
          <p className='error'>*Required! Choose any</p>
        )}
      </div>
    </div>
  );
};

export default Step6Form;
