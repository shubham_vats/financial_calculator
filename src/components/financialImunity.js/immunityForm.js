import React, { useState } from 'react';
import { Formik, Form } from 'formik';
import { Button } from 'reactstrap';
import Step1Form from './step1Form';
import Step2Form from './step2Form';
import Step3Form from './step3Form';
import Step4Form from './step4Form';
import Step5Form from './step5Form.js';
import Step6Form from './step6Form.js';
import Step7Form from './step7Form.js';
import * as Yup from 'yup';

const _renderStepContent = (
  step,
  values,
  setFieldValue,
  handleChange,
  errors,
  touched
) => {
  switch (step) {
    case 0:
      return (
        <Step1Form
          values={values}
          setFieldValue={setFieldValue}
          handleChange={handleChange}
          errors={errors}
          touched={touched}
        />
      );
    case 1:
      return (
        <Step2Form
          values={values}
          setFieldValue={setFieldValue}
          handleChange={handleChange}
          errors={errors}
          touched={touched}
        />
      );
    case 2:
      return (
        <Step3Form
          values={values}
          setFieldValue={setFieldValue}
          handleChange={handleChange}
          errors={errors}
          touched={touched}
        />
      );
    case 3:
      return (
        <Step4Form
          values={values}
          setFieldValue={setFieldValue}
          handleChange={handleChange}
          errors={errors}
          touched={touched}
        />
      );
    case 4:
      return (
        <Step5Form
          values={values}
          setFieldValue={setFieldValue}
          handleChange={handleChange}
          errors={errors}
          touched={touched}
        />
      );
    case 5:
      return (
        <Step6Form
          values={values}
          setFieldValue={setFieldValue}
          handleChange={handleChange}
          errors={errors}
          touched={touched}
        />
      );
    case 6:
      return (
        <Step7Form
          values={values}
          setFieldValue={setFieldValue}
          handleChange={handleChange}
          errors={errors}
          touched={touched}
        />
      );
    default:
      return <div>Not Found</div>;
  }
};

const currentValidationSchema = [
  Yup.object().shape({
    gender: Yup.string().required(),
    salary: Yup.number().required().positive().integer(),
  }),
  Yup.object().shape({
    savings: Yup.number().required().positive().integer(),
    expenditure: Yup.number().required().positive().integer(),
  }),
  Yup.object().shape({
    lifeInsurance: Yup.number().required().positive().integer(),
    healthInsurance: Yup.number().required().positive().integer(),
  }),
  Yup.object().shape({
    // retirement: Yup.number().required().positive().integer(),
  }),
  Yup.object().shape({
    partialChild: Yup.number().positive().integer(),
    completeChild: Yup.number().positive().integer(),
  }),
  Yup.object().shape({
    emergency: Yup.number().required().positive().integer(),
  }),
  Yup.object().shape({
    allFamilyLife: Yup.number().required().positive().integer(),
    allFamilyHealth: Yup.number().required().positive().integer(),
  }),
];

const ImmunityForm = (props) => {
  const [activeStep, setActiveStep] = useState(0);

  const handleSubmit = async (values, actions) => {
    if (activeStep >= 6) {
      props.onsubmit(values, actions);
    } else {
      setActiveStep(activeStep + 1);
    }
  };

  const handleBack = () => {
    setActiveStep(activeStep - 1);
  };

  return (
    <div className='immunity-form'>
      <Formik
        initialValues={{
          gender: '',
          age: '',
          salary: '',
          savings: '',
          expenditure: '',
          lifeInsurance: '',
          healthInsurance: '',
          retirement: '',
          partialChild: '',
          completeChild: '',
          emergency: '',
          allFamilyLife: '',
          allFamilyHealth: '',
        }}
        // validationSchema={currentValidationSchema[activeStep]}
        onSubmit={handleSubmit}
      >
        {({
          values,
          setFieldValue,
          handleSubmit,
          handleChange,
          errors,
          touched,
        }) => (
          <Form>
            {_renderStepContent(
              activeStep,
              values,
              setFieldValue,
              handleChange,
              errors,
              touched
            )}
            <div className='form-button'>
              {activeStep !== 0 && (
                <div>
                  <Button onClick={handleBack}>Back</Button>
                </div>
              )}
              <div>
                <Button
                  // disabled={isSubmitting}
                  type='submit'
                  variant='contained'
                  color='primary'
                >
                  {activeStep >= 6 ? 'Submit' : 'Next'}
                </Button>
              </div>
            </div>
          </Form>
        )}
      </Formik>
    </div>
  );
};

export default ImmunityForm;
