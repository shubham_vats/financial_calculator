import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import LifeInsurance from '../../assets/images/life-insurance.png';
import Healthcare from '../../assets/images/healthcare.png';
import '../../css/financialScore.css';

const Step3Form = (props) => {
  return (
    <div className='step2form'>
      {' '}
      <div className='img'>
        <div>
          <img src={LifeInsurance} />
        </div>
        <div className='img-input'>
          <FormGroup>
            {/* <Label for='exampleSelect'>Choose Salary</Label> */}
            <Input
              type='number'
              name='lifeInsurance'
              className='salary-text'
              value={props.values.lifeInsurance}
              onChange={props.handleChange}
              placeholder='Amount on Life insurance'
            ></Input>
            {props.errors.lifeInsurance && props.touched.lifeInsurance && (
              <p className='error'>*Required</p>
            )}

            {/* <Label>Enter amount you want to spend on Life insurance</Label> */}
          </FormGroup>
        </div>
      </div>
      <div className='img'>
        <div>
          <img src={Healthcare} />
        </div>
        <div className='img-input'>
          <FormGroup>
            {/* <Label for='exampleSelect'>Choose Salary</Label> */}
            <Input
              type='number'
              name='healthInsurance'
              onChange={props.handleChange}
              value={props.values.healthInsurance}
              className='salary-text'
              placeholder='Amount on health insurance'
            ></Input>
            {props.errors.healthInsurance && props.touched.healthInsurance && (
              <p className='error'>*Required</p>
            )}

            {/* <Label>Enter amount you want to spend on health insurance</Label> */}
          </FormGroup>
        </div>
      </div>
    </div>
  );
};

export default Step3Form;
