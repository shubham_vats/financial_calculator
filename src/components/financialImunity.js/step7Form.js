import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import LifeInsurance from '../../assets/images/life-insurance.png';
import Healthcare from '../../assets/images/healthcare.png';
import '../../css/financialScore.css';

const Step7Form = ({ handleChange, values, errors, touched }) => {
  return (
    <div className='step2form'>
      {' '}
      <div className='img'>
        <div>
          <img src={LifeInsurance} />
        </div>
        <div className='img-input'>
          <FormGroup>
            {/* <Label for='exampleSelect'>Choose Salary</Label> */}
            <Input
              type='number'
              name='allFamilyLife'
              className='salary-text'
              onChange={handleChange}
              value={values.allFamilyLife}
              placeholder="Amount on family's Life insurance for family"
            ></Input>
            {errors.allFamilyLife && touched.allFamilyLife && (
              <p className='error'>*Required</p>
            )}

            {/* <Label>Enter amount you want to spend on Life insurance</Label> */}
          </FormGroup>
        </div>
      </div>
      <div className='img'>
        <div>
          <img src={Healthcare} />
        </div>
        <div className='img-input'>
          <FormGroup>
            {/* <Label for='exampleSelect'>Choose Salary</Label> */}
            <Input
              type='number'
              name='allFamilyHealth'
              className='salary-text'
              onChange={handleChange}
              value={values.allFamilyHealth}
              placeholder="Amount on family's health insurance"
            ></Input>
            {errors.allFamilyHealth && touched.allFamilyHealth && (
              <p className='error'>*Required</p>
            )}

            {/* <Label>Enter amount you want to spend on health insurance</Label> */}
          </FormGroup>
        </div>
      </div>
    </div>
  );
};

export default Step7Form;
