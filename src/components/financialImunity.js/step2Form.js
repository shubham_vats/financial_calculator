import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import SaveMoney from '../../assets/images/save-money.png';
import ExpenseMoney from '../../assets/images/expense.png';
import '../../css/financialScore.css';

const Step2Form = (props) => {
  return (
    <div className='step2form'>
      {' '}
      <div className='img'>
        <div>
          <img src={SaveMoney} />
        </div>
        <div className='img-input'>
          <FormGroup>
            {/* <Label for='exampleSelect'>Choose Salary</Label> */}
            <Input
              type='number'
              className='salary-text'
              onChange={(e) => {
                props.setFieldValue('savings', e.target.value);
                props.setFieldValue(
                  'expenditure',
                  props.values.salary - e.target.value
                );
              }}
              value={props.values.savings}
              name='savings'
              placeholder='Enter money to save'
            ></Input>
            {props.errors.savings && props.touched.savings && (
              <p className='error'>*Required</p>
            )}
          </FormGroup>
        </div>
      </div>
      <div className='img'>
        <div>
          <img src={ExpenseMoney} />
        </div>
        <div className='img-input'>
          <FormGroup>
            <Input
              type='number'
              name='expenditure'
              className='salary-text'
              // onChange={props.handleChange}
              onChange={(e) => {
                props.setFieldValue('expenditure', e.target.value);
                props.setFieldValue(
                  'savings',
                  props.values.salary - e.target.value
                );
              }}
              value={props.values.expenditure}
              placeholder='Enter Money for expenditure'
            ></Input>
            {props.errors.expenditure && props.touched.expenditure && (
              <p className='error'>*Required</p>
            )}
            {/* <Label classname='label' for='exampleSelect'>
              Enter money to use for expenditure
            </Label> */}
          </FormGroup>
        </div>
      </div>
    </div>
  );
};

export default Step2Form;
