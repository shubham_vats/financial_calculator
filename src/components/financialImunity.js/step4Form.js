import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import Retirement from '../../assets/images/retirement.png';
import '../../css/financialScore.css';

const Step4Form = ({ handleChange, values, errors, touched }) => {
  return (
    <div className='step2form'>
      {' '}
      <div className='img'>
        <div>
          <img src={Retirement} />
        </div>
        <div className='img-input'>
          <FormGroup>
            <Input
              type='number'
              name='retirement'
              className='salary-text'
              placeholder='Income that you want to save for retirement'
              onChange={handleChange}
              value={values.retirement}
            ></Input>
            {errors.retirement && touched.retirement && (
              <p className='error'>*Required</p>
            )}

            {/* <Label>Enter income that you want to save it for retirement</Label> */}
          </FormGroup>
        </div>
      </div>
    </div>
  );
};

export default Step4Form;
