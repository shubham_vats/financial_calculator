import { useState } from 'react';
import { Button, Form, FormGroup, Label, Input, Col } from 'reactstrap';
import ManImage from '../../assets/images/man (1).png';
import WomanImage from '../../assets/images/woman.png';

import '../../css/financialScore.css';
const Step1Form = ({
  values,
  errors,
  setFieldValue,
  handleChange,
  touched,
}) => {
  const [gender, setGender] = useState(values?.gender);
  return (
    <div className='container'>
      {' '}
      <div className='avatar'>
        <div className='avatar-img'>
          <div
            onClick={() => {
              setFieldValue('gender', 'male');
              setGender('male');
            }}
            className={gender === 'male' && 'active'}
          >
            <img src={ManImage} alt='men' />
          </div>
          <div
            onClick={() => {
              setFieldValue('gender', 'female');
              setGender('female');
            }}
            className={gender === 'female' && 'active'}
          >
            <img src={WomanImage} alt='women' />
          </div>
        </div>
        {errors.gender && touched.gender && (
          <p className='error'>*Avatar is required</p>
        )}
        <span className='avatar-text'>Choose Your Avatar</span>
      </div>
      <Col sm='12' md={{ size: 6, offset: 3 }}>
        <FormGroup>
          <Input
            type='number'
            name='age'
            placeholder='Enter age'
            onChange={handleChange}
            values={values.age}
          />
          {errors.salary && touched.salary && (
            <p className='error'>*Age is required</p>
          )}
        </FormGroup>
      </Col>
      <br />
      <Col sm='12' md={{ size: 6, offset: 3 }}>
        <FormGroup>
          <Input
            type='number'
            name='salary'
            placeholder='Enter salary to proceed'
            onChange={handleChange}
            values={values.salary}
          />
          {errors.salary && touched.salary && (
            <p className='error'>*Salary is required</p>
          )}
        </FormGroup>
      </Col>
    </div>
  );
};

export default Step1Form;
