import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import Child from '../../assets/images/child.png';
import '../../css/financialScore.css';

const Step5Form = ({ handleChange, values, errors, touched }) => {
  return (
    <div className='step2form'>
      {' '}
      <div className='child-img'>
        <div>
          <img src={Child} />
        </div>
        <div className='img-input'>
          <Input
            type='number'
            name='partialChild'
            className='salary-text'
            onChange={handleChange}
            value={values.partialChild}
            disabled={values.completeChild ? true : false}
            placeholder='Amount to partially secure child’s future'
          ></Input>
          {/* <Label>Enter amount to partially secure child’s future Enter amount to Completely secure</Label> */}
          {errors.partialChild && touched.partialChild && (
            <p className='error'>*Required</p>
          )}

          <Input
            type='number'
            name='completeChild'
            className='salary-text'
            onChange={handleChange}
            disabled={values.partialChild ? true : false}
            value={values.completeChild}
            placeholder="Amount to Completely secure child's future"
          ></Input>
          {errors.completeChild && touched.completeChild && (
            <p className='error'>*Required</p>
          )}

          {/* <Label>Enter income that you want to save it for retirement</Label> */}
        </div>
      </div>
    </div>
  );
};

export default Step5Form;
