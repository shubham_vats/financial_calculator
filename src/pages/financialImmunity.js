
import React, { useEffect, useState } from 'react'
import { Col, Container, Form, Row, Button } from 'react-bootstrap'
import { useForm } from "react-hook-form";
import WheelComponent from 'react-wheel-of-prizes'
import '../css/financialImmunity.css'
import 'react-wheel-of-prizes/dist/index.css'
import { HLV_COVER } from '../constant/hlvCover'

const segColors = [
  '#EE4040',
  '#F0CF50',
  '#815CD1',
  '#3DA5E0',
  '#34A24F',
  '#F9AA1F',
  '#EC3F3F',
  '#FF9000'
]

const logic = ({ annualIncome, monthlyExpenses, extInsuranceCover, age, retirementAge }) => {

  // const annualIncome = 900000
  // const monthlyExpenses = 12000
  // const extInsuranceCover = 50000
  // const age = 25
  // const retirementAge = 40

  const surplusA = annualIncome - (monthlyExpenses * 12) - extInsuranceCover;
  let surplusACopy = surplusA

  const incomeArray = [surplusA].concat(Array(retirementAge - age).fill().map(item => {
    return Math.round(surplusACopy *= 1.064)
  }))


  let discoutFactor = []
  Array(retirementAge - age).fill().map((x, item) => {
    const root = 1 / 1.05
    if (discoutFactor.length === 0) {
      discoutFactor.push(root)
    }
    discoutFactor.push(root * discoutFactor[item])

  })

  const futureIncome = incomeArray.map((x, i) => x * discoutFactor[i]).reduce((a, b) => a + b, 0)

  console.log(Math.round(futureIncome));
  return Math.round(futureIncome)
}

const FinancialImmunity = () => {
  const [segments, setSegment] = useState(["1", "2", "3", "4", "5", "6", "7", "8"])
  const [key, setKey] = useState(Date.now())
  const [closestLifeCover, setClosetLifeCover] = useState({ "closestLifeCover.lifeCover": 5 })
  const [allData, setAllData] = useState({})
  const [lifeCover, setLifeCover] = useState(0)

  const { register, handleSubmit } = useForm();
  const { register: register2, handleSubmit: handleSubmit2 } = useForm();

  const onSubmit = data => {
    const out = calculateDummy(data)
    setSegment(out)
    setAllData({ ...data })
    setKey(Date.now())
  };

  const onExtraDetailSubmit = data => {
    const dd = allData
    // setAllData({ ...dd, ...data })
    const lifeCoverValue = logic({ ...dd, ...data })
    setLifeCover(lifeCoverValue)
    // "age":"25","annualIncome":"600000","retirementAge":"40","monthlyExpenses":"12000","extInsuranceCover":"50000"

  }

  const calculateDummy = ({ age, annualIncome }) => {
    const sortedCover = HLV_COVER.filter(item => item.Age === age)
    const closest = sortedCover.reduce(function (prev, curr) {
      return (Math.abs(curr.Income - annualIncome) < Math.abs(prev.Income - annualIncome) ? curr : prev);
    });
    setClosetLifeCover(closest)

    const { Age, Income, lifeCover } = closest
    const thirdPart = lifeCover / 3
    const stepNumber = thirdPart / 2
    const startNumber = lifeCover - thirdPart;
    const endNumber = lifeCover + thirdPart
    // return { stepNumber, startNumber, endNumber }
    return range(startNumber, endNumber, stepNumber);
  }

  const range = (start, end, step) => {
    return Array(10).fill().map((x, i) => {
      return Math.round((start + (step * (i + 1)))).toString()
    })
  }

  const onFinished = (winner) => {
    console.log(winner)
  }

  useEffect(() => {
    // canvas
    const parent = document.getElementById("parent");
    const canvas = document.getElementById('canvas');
    const context = canvas.getContext('2d');
    window.addEventListener('resize', resizeCanvas, false);
    function resizeCanvas() {
      let W = canvas.width, H = canvas.height
      let temp = context.getImageData(0, 0, W, H)
      context.canvas.width = parent.offsetWidth;
      context.canvas.height = parent.offsetHeight;

      context.putImageData(temp, 0, 0)
    }
    resizeCanvas();
  })

  return (
    < >
      <Row>
        <h2>Wheel of Fortune</h2>
        <Col>
          <div className="divWala" id="parent" key={key}>
            <WheelComponent
              segments={segments}
              segColors={segColors}
              winningSegment={closestLifeCover.lifeCover}
              onFinished={(winner) => onFinished(winner)}
              primaryColor='black'
              contrastColor='white'
              buttonText='Click to Spin'
              isOnlyOnce={false}
              size={200}
              upDuration={100}
              downDuration={2000}
            />
          </div>
        </Col>

        <Col>
          <div className="form">
            <Form onSubmit={handleSubmit(onSubmit)} >
              <Row>
                <Col>
                  <Form.Control placeholder="Age" {...register("age", { required: true })} />
                </Col>
                <Col>
                  <Form.Control placeholder="Annual Income"  {...register("annualIncome", { required: true })} />
                </Col>
              </Row>
              <br />
              <div className="d-grid gap-2">
                <Button variant="primary" size="sm" type="submit">
                  Submit Details
                </Button>
              </div>
            </Form>
            <br />
            <h6>Enter more details to check life cover</h6>
            <Form onSubmit={handleSubmit2(onExtraDetailSubmit)}>
              <Row>
                <Col>
                  <Form.Control placeholder="Retirement Age" {...register2("retirementAge", { required: true })} />
                </Col>
                <Col>
                  <Form.Control placeholder="Monthly Expenses"  {...register2("monthlyExpenses", { required: true })} />
                </Col>
              </Row>
              <br />
              <Row>
                <Col>
                  <Form.Control placeholder="Existing Insurance Cover"  {...register2("extInsuranceCover", { required: true })} />
                </Col>
              </Row>
              <br />
              <div className="d-grid gap-2">
                <Button variant="info" size="sm" type="submit">
                  Check Life Cover
                </Button>
              </div>
            </Form>
            <br />
            <h5 className="text-center">Your Total Life Cover Value is Rs {lifeCover}</h5>
          </div>
        </Col>
      </Row>
    </ >

  )
}


export default FinancialImmunity;