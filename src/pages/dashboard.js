import App, { history } from '../App';
import "../css/dashboard.css"
import { Button } from 'reactstrap';
import { Container, Row, Col } from 'react-bootstrap';

const Dashboard = () => {
  return (
    <div className="dashboard">
      <Container>
        <Row className='dashboard-title'>
          <h1>Team Arrow</h1>
        </Row>
        <br />
        <Row>
          <Col className="d-grid gap-2">
            <Button
              color='primary'
              className='btn'
              onClick={() => {
                history.push('/human-life-cover');
              }}
            >
              Life Cover
            </Button>
          </Col>
          <Col className="d-grid gap-2">
            <Button
              size="lg"
              color='warning'
              onClick={() => {
                history.push('/financial-immunity');
              }}
            >
              Financial Immunity
            </Button>
          </Col>
          <Col className="d-grid gap-2">
            <Button
              color='info'
              onClick={() => {
                history.push('/partner-api');
              }}
            >
              Partner API
            </Button>
          </Col>
        </Row>
        {/* <br />
        <Row className='dashboard-title'>
          <small>Powered by: ADCAT(HDFC LIFE)</small>
        </Row> */}
      </Container>

    </div>
  );
};

export default Dashboard;
