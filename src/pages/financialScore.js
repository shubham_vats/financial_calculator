import ImmunityForm from '../components/financialImunity.js/immunityForm';
import '../css/financialScore.css';
import React, { useState } from 'react';
import {
  checkOption,
  emergencyConstant,
  retirementConstat,
  savingsConstant,
  ageConstant
} from '../components/constantData';
import { Card, CardText, CardTitle, Button, CardSubtitle } from 'reactstrap';
import ManImage from '../assets/images/man (1).png';
import WomanImage from '../assets/images/woman.png';
let url = "http://localhost:8082/v1/financial-immunity-calculator"


const colors = ['secondary', 'success'];

const FinancialScore = () => {
  const [financialScoreObj, setFinancialScore] = useState({
    Score: 0,
    SuggestedProduct: [],
  });
  const [remainingAmount, setRemainingAmount] = useState('');
  const [gender, setGender] = useState('');
  onsubmit = (values) => {
    console.log('value we receieved', values);
    const {
      allFamilyHealth: allFamilyHealthValue,
      allFamilyLife: allFamilyLifeValue,
      completeChild,
      emergency: emergencyuptoMonth,
      expenditure,
      gender,
      healthInsurance,
      lifeInsurance,
      partialChild,
      retirement: retirementSavings,
      salary,
      savings: monthlySavings,
      age: ageFromUser
    } = values;

    // calculate remaining amount

    let remaining =
      values.expenditure -
      values.allFamilyHealth -
      values.allFamilyLife -
      values.completeChild -
      values.healthInsurance -
      values.lifeInsurance -
      values.partialChild -
      values.retirement;

    setRemainingAmount(remaining);
    setGender(values.gender);
    // calculate Percentages
    const savingsPer = (monthlySavings * 100) / salary;
    const retirementPer = (retirementSavings * 100) / salary;
    const expenditurePer = (expenditure * 100) / salary;
    const allFamilyHealth = allFamilyHealthValue > 0 ? 'A' : 'B';
    const allFamilyLife = allFamilyLifeValue > 0 ? 'A' : 'B';

    const child =
      completeChild > 0 && partialChild > 0
        ? 'D'
        : completeChild > 0 && partialChild <= 0
          ? 'B'
          : completeChild <= 0 && partialChild > 0
            ? 'C'
            : 'A';

    const healthAndLifeInsurance =
      healthInsurance > 0 && lifeInsurance > 0
        ? 'D'
        : healthInsurance > 0 && lifeInsurance <= 0
          ? 'B'
          : healthInsurance <= 0 && lifeInsurance > 0
            ? 'C'
            : 'A';

    console.log({ retirementPer });
    // check options
    const savings = checkOption(savingsConstant, savingsPer);
    const retirement = checkOption(retirementConstat, retirementPer);
    const emergency = checkOption(emergencyConstant, emergencyuptoMonth);
    const age = checkOption(ageConstant, ageFromUser)

    console.log("savings", savings.option)
    console.log("retirement", retirement.option)
    console.log("emergency", emergency.option)
    console.log("age", age.option)

    const apiData = {
      allFamilyHealth,
      allFamilyLife,
      child,
      healthAndLifeInsurance,
      savings: savings.option,
      retirement: retirement.option,
      emergency: emergency.option,
      age: age.option
    };
    console.log('apiData', apiData);
    fetch(url, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(apiData),
    })
      .then((response) => response.json())
      .then(({ data }) => {
        console.log('Success:', data);
        setFinancialScore(data);
      })
      .catch((error) => {
        console.error('Error:', error);
      });
  };
  return (
    <div className='financialScore'>
      {financialScoreObj.Score ? (
        <div className='final-page'>
          <div className='avatar'>
            <img src={gender === 'male' ? ManImage : WomanImage} alt='avatar' />
            <p>
              <b>Remaining Amount</b>
            </p>
            <h3>{remainingAmount}</h3>
          </div>
          <div className='text-center align-middle  col-4'>
            <h3>Financial Immunity Result</h3>
            <Card body>
              <CardTitle tag='h5'>
                Your Financial risk score is <b>{financialScoreObj.Score}</b>
              </CardTitle>
              <CardText>Please Click on below suggested Products</CardText>
              {financialScoreObj.SuggestedProduct.map((item, i) => {
                return (
                  <>
                    <Button key={i} color={colors[i]}>
                      {item}
                    </Button>{' '}
                    <br />
                  </>
                );
              })}
            </Card>
            <div className='plan'>
              <Button
                color='warning'
                onClick={() => {
                  setFinancialScore('');
                }}
              >
                Start again
              </Button>
            </div>
          </div>
        </div>
      ) : (
        <ImmunityForm onsubmit={onsubmit} />
      )}
    </div>
  );
};

export default FinancialScore;
