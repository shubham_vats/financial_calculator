import React, { useState } from 'react';
import { Row, Container, Form, Button, Col, Alert } from 'react-bootstrap';
import { useForm } from "react-hook-form";
import "../css/partner.css"

const URL = 'http://localhost:8082/v1/partner'

const PartnerComponent = () => {
    const { register, handleSubmit } = useForm();
    const [key, setKey] = useState("")
    const [errorMessages, setErrorMessages] = useState({})
    const copyKey = () => {
        console.log("copied");
    }

    const submitForm = (apiData) => {
        console.log(apiData);
        setErrorMessages({})
        fetch(URL, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(apiData)
        }).then(response => response.json())
            .then(data => {
                console.log('Success:', data);
                setKey(data.clientId);
                if (data.statusCode === 400) {
                    console.log("errorMessage", data);
                    setErrorMessages(data)
                }
            })
            .catch((error) => {
                console.error('Error:', error);
            });
    }

    return (
        < div className="partner" >
            {console.log(errorMessages)}
            <div className="partner-box">
                <h4>REGISTER PARTNER DETAILS</h4>
                <Row>
                    <Form onSubmit={handleSubmit(submitForm)}>
                        <Row>
                            <Col>
                                <Form.Group className="mb-6" controlId="">
                                    <Form.Control type="text" placeholder="Partner Name" {...register('partnerName', { required: true })} />
                                </Form.Group>
                            </Col>
                            <Col>
                                <Form.Group className="mb-6" controlId="">
                                    <Form.Control type="email" placeholder="Partner Email"  {...register('partnerEmail', { required: true })} />
                                </Form.Group>
                            </Col>
                        </Row>
                        <br />
                        {
                            errorMessages.statusCode === 400 &&
                            <p className="error">
                                {errorMessages.errorMessage}
                            </p>
                        }
                        {
                            key &&
                            <p className="sucess">Your Api Key Successfully Sent on Email.</p>
                        }
                        <Row>
                            <Col>
                                <Button variant="primary" type="submit">
                                    Get Partner Key
                                </Button>
                            </Col>
                        </Row>
                    </Form>
                </Row>
                <br />
                <br />
                {
                    key && <Row>
                        <p> {key}   &nbsp;&nbsp;<Button size="sm" variant="outline-success" onClick={() => copyKey}> Copy key</Button></p>
                    </Row>
                }
            </div>
        </div >
    );
}

export default PartnerComponent;