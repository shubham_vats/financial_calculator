export const HLV_COVER = [
    {
        "Age": "22",
        "Income": "600000",
        "Graduate": "FALSE",
        "lifeCover": "10000000"
    },
    {
        "Age": "23",
        "Income": "500000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "23",
        "Income": "600000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "24",
        "Income": "350000",
        "Graduate": "TRUE",
        "lifeCover": "8760000"
    },
    {
        "Age": "24",
        "Income": "500000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "24",
        "Income": "700000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "24",
        "Income": "800000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "25",
        "Income": "500000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "25",
        "Income": "550000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "25",
        "Income": "600000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "25",
        "Income": "1600000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "25",
        "Income": "10000000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "26",
        "Income": "365000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "26",
        "Income": "500000",
        "Graduate": "FALSE",
        "lifeCover": "10000000"
    },
    {
        "Age": "26",
        "Income": "500000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "26",
        "Income": "525000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "26",
        "Income": "550000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "26",
        "Income": "595000",
        "Graduate": "TRUE",
        "lifeCover": "9800000"
    },
    {
        "Age": "26",
        "Income": "600000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "26",
        "Income": "700000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "26",
        "Income": "728048",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "26",
        "Income": "1154098",
        "Graduate": "TRUE",
        "lifeCover": "9700000"
    },
    {
        "Age": "26",
        "Income": "1200000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "26",
        "Income": "1300000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "26",
        "Income": "1500000",
        "Graduate": "TRUE",
        "lifeCover": "9669176"
    },
    {
        "Age": "26",
        "Income": "1600000",
        "Graduate": "TRUE",
        "lifeCover": "9750000"
    },
    {
        "Age": "27",
        "Income": "260000",
        "Graduate": "TRUE",
        "lifeCover": "7800000"
    },
    {
        "Age": "27",
        "Income": "400000",
        "Graduate": "TRUE",
        "lifeCover": "8100000"
    },
    {
        "Age": "27",
        "Income": "500000",
        "Graduate": "TRUE",
        "lifeCover": "9875000"
    },
    {
        "Age": "27",
        "Income": "600000",
        "Graduate": "TRUE",
        "lifeCover": "9604388.875"
    },
    {
        "Age": "27",
        "Income": "800000",
        "Graduate": "FALSE",
        "lifeCover": "9181200"
    },
    {
        "Age": "27",
        "Income": "900000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "27",
        "Income": "1200000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "27",
        "Income": "2000000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "28",
        "Income": "500000",
        "Graduate": "TRUE",
        "lifeCover": "9743160.5"
    },
    {
        "Age": "28",
        "Income": "540000",
        "Graduate": "TRUE",
        "lifeCover": "8720000"
    },
    {
        "Age": "28",
        "Income": "600000",
        "Graduate": "TRUE",
        "lifeCover": "5225000"
    },
    {
        "Age": "28",
        "Income": "650000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "28",
        "Income": "700000",
        "Graduate": "TRUE",
        "lifeCover": "8715000"
    },
    {
        "Age": "28",
        "Income": "800000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "28",
        "Income": "850000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "28",
        "Income": "964000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "28",
        "Income": "1180000",
        "Graduate": "TRUE",
        "lifeCover": "9600000"
    },
    {
        "Age": "28",
        "Income": "1200000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "28",
        "Income": "1250000",
        "Graduate": "TRUE",
        "lifeCover": "8860000"
    },
    {
        "Age": "28",
        "Income": "1600000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "28",
        "Income": "2000000",
        "Graduate": "TRUE",
        "lifeCover": "9400000"
    },
    {
        "Age": "28",
        "Income": "5000000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "29",
        "Income": "500000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "29",
        "Income": "540000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "29",
        "Income": "550000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "29",
        "Income": "600000",
        "Graduate": "TRUE",
        "lifeCover": "9643480.333"
    },
    {
        "Age": "29",
        "Income": "624000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "29",
        "Income": "650000",
        "Graduate": "TRUE",
        "lifeCover": "8740000"
    },
    {
        "Age": "29",
        "Income": "700000",
        "Graduate": "TRUE",
        "lifeCover": "9370000"
    },
    {
        "Age": "29",
        "Income": "732000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "29",
        "Income": "800000",
        "Graduate": "TRUE",
        "lifeCover": "9933333.333"
    },
    {
        "Age": "29",
        "Income": "811610",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "29",
        "Income": "850000",
        "Graduate": "TRUE",
        "lifeCover": "9085600"
    },
    {
        "Age": "29",
        "Income": "900000",
        "Graduate": "TRUE",
        "lifeCover": "8870050"
    },
    {
        "Age": "29",
        "Income": "980000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "29",
        "Income": "1000000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "29",
        "Income": "1050000",
        "Graduate": "TRUE",
        "lifeCover": "9780000"
    },
    {
        "Age": "29",
        "Income": "1300000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "29",
        "Income": "1400000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "29",
        "Income": "1500000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "29",
        "Income": "1800000",
        "Graduate": "TRUE",
        "lifeCover": "8514981"
    },
    {
        "Age": "29",
        "Income": "2000000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "29",
        "Income": "2835273",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "29",
        "Income": "3000000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "29",
        "Income": "10000000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "30",
        "Income": "450000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "30",
        "Income": "500000",
        "Graduate": "TRUE",
        "lifeCover": "9392146.818"
    },
    {
        "Age": "30",
        "Income": "600000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "30",
        "Income": "700000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "30",
        "Income": "750000",
        "Graduate": "TRUE",
        "lifeCover": "9872185"
    },
    {
        "Age": "30",
        "Income": "780000",
        "Graduate": "TRUE",
        "lifeCover": "8923310"
    },
    {
        "Age": "30",
        "Income": "1000000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "30",
        "Income": "1127070",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "30",
        "Income": "1200000",
        "Graduate": "TRUE",
        "lifeCover": "8940000"
    },
    {
        "Age": "30",
        "Income": "1400000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "30",
        "Income": "1500000",
        "Graduate": "TRUE",
        "lifeCover": "5687862"
    },
    {
        "Age": "30",
        "Income": "1600000",
        "Graduate": "TRUE",
        "lifeCover": "8720000"
    },
    {
        "Age": "30",
        "Income": "3500000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "30",
        "Income": "6500000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "31",
        "Income": "500000",
        "Graduate": "TRUE",
        "lifeCover": "8577000"
    },
    {
        "Age": "31",
        "Income": "550000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "31",
        "Income": "600000",
        "Graduate": "TRUE",
        "lifeCover": "9183443.25"
    },
    {
        "Age": "31",
        "Income": "690000",
        "Graduate": "TRUE",
        "lifeCover": "8500000"
    },
    {
        "Age": "31",
        "Income": "694000",
        "Graduate": "TRUE",
        "lifeCover": "8500000"
    },
    {
        "Age": "31",
        "Income": "700000",
        "Graduate": "TRUE",
        "lifeCover": "9515016"
    },
    {
        "Age": "31",
        "Income": "750000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "31",
        "Income": "759722",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "31",
        "Income": "800000",
        "Graduate": "TRUE",
        "lifeCover": "9750000"
    },
    {
        "Age": "31",
        "Income": "900000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "31",
        "Income": "1000000",
        "Graduate": "FALSE",
        "lifeCover": "10000000"
    },
    {
        "Age": "31",
        "Income": "1000000",
        "Graduate": "TRUE",
        "lifeCover": "8750000"
    },
    {
        "Age": "31",
        "Income": "1050000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "31",
        "Income": "1150000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "31",
        "Income": "1200000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "31",
        "Income": "1350000",
        "Graduate": "TRUE",
        "lifeCover": "7437500"
    },
    {
        "Age": "31",
        "Income": "1400000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "31",
        "Income": "1500000",
        "Graduate": "TRUE",
        "lifeCover": "7419673"
    },
    {
        "Age": "31",
        "Income": "1800000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "32",
        "Income": "500000",
        "Graduate": "FALSE",
        "lifeCover": "10000000"
    },
    {
        "Age": "32",
        "Income": "500000",
        "Graduate": "TRUE",
        "lifeCover": "9847320"
    },
    {
        "Age": "32",
        "Income": "550000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "32",
        "Income": "600000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "32",
        "Income": "700000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "32",
        "Income": "728532",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "32",
        "Income": "800000",
        "Graduate": "TRUE",
        "lifeCover": "8628595.5"
    },
    {
        "Age": "32",
        "Income": "850000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "32",
        "Income": "1000000",
        "Graduate": "TRUE",
        "lifeCover": "6566000"
    },
    {
        "Age": "32",
        "Income": "1200000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "32",
        "Income": "1300000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "32",
        "Income": "1400000",
        "Graduate": "TRUE",
        "lifeCover": "9380000"
    },
    {
        "Age": "32",
        "Income": "1500000",
        "Graduate": "TRUE",
        "lifeCover": "8418648"
    },
    {
        "Age": "32",
        "Income": "1800000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "32",
        "Income": "2000000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "33",
        "Income": "350000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "33",
        "Income": "500000",
        "Graduate": "TRUE",
        "lifeCover": "9344444.444"
    },
    {
        "Age": "33",
        "Income": "550000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "33",
        "Income": "570000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "33",
        "Income": "600000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "33",
        "Income": "635000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "33",
        "Income": "650000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "33",
        "Income": "700000",
        "Graduate": "TRUE",
        "lifeCover": "9907462"
    },
    {
        "Age": "33",
        "Income": "720000",
        "Graduate": "TRUE",
        "lifeCover": "8166666.667"
    },
    {
        "Age": "33",
        "Income": "750000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "33",
        "Income": "800000",
        "Graduate": "TRUE",
        "lifeCover": "9460588.667"
    },
    {
        "Age": "33",
        "Income": "850000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "33",
        "Income": "1000000",
        "Graduate": "TRUE",
        "lifeCover": "9184687.5"
    },
    {
        "Age": "33",
        "Income": "1088815",
        "Graduate": "TRUE",
        "lifeCover": "9500000"
    },
    {
        "Age": "33",
        "Income": "1200000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "33",
        "Income": "1500000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "33",
        "Income": "1600000",
        "Graduate": "FALSE",
        "lifeCover": "10000000"
    },
    {
        "Age": "33",
        "Income": "1690000",
        "Graduate": "TRUE",
        "lifeCover": "6200000"
    },
    {
        "Age": "33",
        "Income": "1745261",
        "Graduate": "TRUE",
        "lifeCover": "8300000"
    },
    {
        "Age": "33",
        "Income": "1780000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "33",
        "Income": "2000000",
        "Graduate": "TRUE",
        "lifeCover": "8920000"
    },
    {
        "Age": "33",
        "Income": "2200000",
        "Graduate": "TRUE",
        "lifeCover": "18750000"
    },
    {
        "Age": "33",
        "Income": "2500000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "33",
        "Income": "2900000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "34",
        "Income": "500000",
        "Graduate": "TRUE",
        "lifeCover": "9420000"
    },
    {
        "Age": "34",
        "Income": "550000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "34",
        "Income": "600000",
        "Graduate": "TRUE",
        "lifeCover": "9666666.667"
    },
    {
        "Age": "34",
        "Income": "650000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "34",
        "Income": "700000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "34",
        "Income": "720000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "34",
        "Income": "800000",
        "Graduate": "TRUE",
        "lifeCover": "9733938.333"
    },
    {
        "Age": "34",
        "Income": "1000000",
        "Graduate": "TRUE",
        "lifeCover": "8446666.667"
    },
    {
        "Age": "34",
        "Income": "1100000",
        "Graduate": "TRUE",
        "lifeCover": "9885162"
    },
    {
        "Age": "34",
        "Income": "1200000",
        "Graduate": "TRUE",
        "lifeCover": "9273046.667"
    },
    {
        "Age": "34",
        "Income": "1300000",
        "Graduate": "TRUE",
        "lifeCover": "5220000"
    },
    {
        "Age": "34",
        "Income": "1400000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "34",
        "Income": "1500000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "34",
        "Income": "1716000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "34",
        "Income": "1750000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "34",
        "Income": "2000000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "34",
        "Income": "2700000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "34",
        "Income": "10000000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "35",
        "Income": "500000",
        "Graduate": "TRUE",
        "lifeCover": "9277089.6"
    },
    {
        "Age": "35",
        "Income": "520000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "35",
        "Income": "550000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "35",
        "Income": "600000",
        "Graduate": "TRUE",
        "lifeCover": "9850000"
    },
    {
        "Age": "35",
        "Income": "650000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "35",
        "Income": "665850",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "35",
        "Income": "700000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "35",
        "Income": "720000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "35",
        "Income": "800000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "35",
        "Income": "840000",
        "Graduate": "TRUE",
        "lifeCover": "7600000"
    },
    {
        "Age": "35",
        "Income": "1000000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "35",
        "Income": "1200000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "35",
        "Income": "1451347",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "35",
        "Income": "1525000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "35",
        "Income": "2000000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "35",
        "Income": "2200000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "35",
        "Income": "2500000",
        "Graduate": "TRUE",
        "lifeCover": "9750000"
    },
    {
        "Age": "35",
        "Income": "3000000",
        "Graduate": "TRUE",
        "lifeCover": "9142133.333"
    },
    {
        "Age": "35",
        "Income": "3500000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "35",
        "Income": "4800000",
        "Graduate": "TRUE",
        "lifeCover": "8900000"
    },
    {
        "Age": "35",
        "Income": "25000000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "36",
        "Income": "500000",
        "Graduate": "TRUE",
        "lifeCover": "7500000"
    },
    {
        "Age": "36",
        "Income": "535000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "36",
        "Income": "580000",
        "Graduate": "TRUE",
        "lifeCover": "7500000"
    },
    {
        "Age": "36",
        "Income": "600000",
        "Graduate": "TRUE",
        "lifeCover": "8187548.667"
    },
    {
        "Age": "36",
        "Income": "700000",
        "Graduate": "TRUE",
        "lifeCover": "8415545"
    },
    {
        "Age": "36",
        "Income": "766000",
        "Graduate": "TRUE",
        "lifeCover": "9901460"
    },
    {
        "Age": "36",
        "Income": "800000",
        "Graduate": "TRUE",
        "lifeCover": "8053306.667"
    },
    {
        "Age": "36",
        "Income": "900000",
        "Graduate": "TRUE",
        "lifeCover": "8776330.5"
    },
    {
        "Age": "36",
        "Income": "950000",
        "Graduate": "TRUE",
        "lifeCover": "7500000"
    },
    {
        "Age": "36",
        "Income": "1000000",
        "Graduate": "TRUE",
        "lifeCover": "8537642.857"
    },
    {
        "Age": "36",
        "Income": "1200000",
        "Graduate": "TRUE",
        "lifeCover": "8757080"
    },
    {
        "Age": "36",
        "Income": "1300000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "36",
        "Income": "1400000",
        "Graduate": "TRUE",
        "lifeCover": "9250000"
    },
    {
        "Age": "36",
        "Income": "1489090",
        "Graduate": "TRUE",
        "lifeCover": "7400000"
    },
    {
        "Age": "36",
        "Income": "2000000",
        "Graduate": "TRUE",
        "lifeCover": "9286000"
    },
    {
        "Age": "36",
        "Income": "2500000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "36",
        "Income": "3500000",
        "Graduate": "TRUE",
        "lifeCover": "9488750"
    },
    {
        "Age": "37",
        "Income": "500000",
        "Graduate": "TRUE",
        "lifeCover": "7474938.625"
    },
    {
        "Age": "37",
        "Income": "600000",
        "Graduate": "TRUE",
        "lifeCover": "7379000"
    },
    {
        "Age": "37",
        "Income": "657485",
        "Graduate": "TRUE",
        "lifeCover": "7500000"
    },
    {
        "Age": "37",
        "Income": "700000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "37",
        "Income": "720000",
        "Graduate": "TRUE",
        "lifeCover": "7500000"
    },
    {
        "Age": "37",
        "Income": "800000",
        "Graduate": "TRUE",
        "lifeCover": "8715506.667"
    },
    {
        "Age": "37",
        "Income": "900000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "37",
        "Income": "1000000",
        "Graduate": "TRUE",
        "lifeCover": "6677103.333"
    },
    {
        "Age": "37",
        "Income": "1100000",
        "Graduate": "TRUE",
        "lifeCover": "6500000"
    },
    {
        "Age": "37",
        "Income": "1200000",
        "Graduate": "TRUE",
        "lifeCover": "9743750"
    },
    {
        "Age": "37",
        "Income": "1300000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "37",
        "Income": "1400000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "37",
        "Income": "1500000",
        "Graduate": "TRUE",
        "lifeCover": "9487500"
    },
    {
        "Age": "37",
        "Income": "1550000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "37",
        "Income": "1800000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "37",
        "Income": "2000000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "37",
        "Income": "2200000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "37",
        "Income": "2600000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "37",
        "Income": "3000000",
        "Graduate": "TRUE",
        "lifeCover": "8785364"
    },
    {
        "Age": "37",
        "Income": "6500000",
        "Graduate": "TRUE",
        "lifeCover": "7500000"
    },
    {
        "Age": "38",
        "Income": "500000",
        "Graduate": "TRUE",
        "lifeCover": "7500000"
    },
    {
        "Age": "38",
        "Income": "543000",
        "Graduate": "TRUE",
        "lifeCover": "7500000"
    },
    {
        "Age": "38",
        "Income": "600000",
        "Graduate": "TRUE",
        "lifeCover": "7091738"
    },
    {
        "Age": "38",
        "Income": "700000",
        "Graduate": "TRUE",
        "lifeCover": "8593250"
    },
    {
        "Age": "38",
        "Income": "780000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "38",
        "Income": "865000",
        "Graduate": "TRUE",
        "lifeCover": "9000000"
    },
    {
        "Age": "38",
        "Income": "1000000",
        "Graduate": "TRUE",
        "lifeCover": "8330000"
    },
    {
        "Age": "38",
        "Income": "1100000",
        "Graduate": "TRUE",
        "lifeCover": "7500000"
    },
    {
        "Age": "38",
        "Income": "1200000",
        "Graduate": "TRUE",
        "lifeCover": "9377576.5"
    },
    {
        "Age": "38",
        "Income": "1475000",
        "Graduate": "TRUE",
        "lifeCover": "8270000"
    },
    {
        "Age": "38",
        "Income": "1500000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "38",
        "Income": "1566000",
        "Graduate": "TRUE",
        "lifeCover": "7743736"
    },
    {
        "Age": "38",
        "Income": "1600000",
        "Graduate": "TRUE",
        "lifeCover": "6994370"
    },
    {
        "Age": "38",
        "Income": "2000000",
        "Graduate": "TRUE",
        "lifeCover": "9400000"
    },
    {
        "Age": "38",
        "Income": "2300000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "38",
        "Income": "3200000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "38",
        "Income": "5000000",
        "Graduate": "TRUE",
        "lifeCover": "7500000"
    },
    {
        "Age": "39",
        "Income": "100000",
        "Graduate": "TRUE",
        "lifeCover": "7500000"
    },
    {
        "Age": "39",
        "Income": "500000",
        "Graduate": "TRUE",
        "lifeCover": "7500000"
    },
    {
        "Age": "39",
        "Income": "508836",
        "Graduate": "TRUE",
        "lifeCover": "7500000"
    },
    {
        "Age": "39",
        "Income": "520485",
        "Graduate": "TRUE",
        "lifeCover": "6500000"
    },
    {
        "Age": "39",
        "Income": "600000",
        "Graduate": "TRUE",
        "lifeCover": "8125000"
    },
    {
        "Age": "39",
        "Income": "650000",
        "Graduate": "TRUE",
        "lifeCover": "7500000"
    },
    {
        "Age": "39",
        "Income": "700000",
        "Graduate": "TRUE",
        "lifeCover": "9600440"
    },
    {
        "Age": "39",
        "Income": "713000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "39",
        "Income": "800000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "39",
        "Income": "890000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "39",
        "Income": "900000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "39",
        "Income": "1000000",
        "Graduate": "TRUE",
        "lifeCover": "8180000"
    },
    {
        "Age": "39",
        "Income": "1200000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "39",
        "Income": "1400000",
        "Graduate": "TRUE",
        "lifeCover": "7500000"
    },
    {
        "Age": "39",
        "Income": "1500000",
        "Graduate": "TRUE",
        "lifeCover": "9047673"
    },
    {
        "Age": "39",
        "Income": "1750000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "39",
        "Income": "2200000",
        "Graduate": "TRUE",
        "lifeCover": "7500000"
    },
    {
        "Age": "39",
        "Income": "2500000",
        "Graduate": "TRUE",
        "lifeCover": "9400000"
    },
    {
        "Age": "39",
        "Income": "2930000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "39",
        "Income": "3000000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "40",
        "Income": "300000",
        "Graduate": "TRUE",
        "lifeCover": "7500000"
    },
    {
        "Age": "40",
        "Income": "500000",
        "Graduate": "TRUE",
        "lifeCover": "7315007.167"
    },
    {
        "Age": "40",
        "Income": "510260",
        "Graduate": "TRUE",
        "lifeCover": "7500000"
    },
    {
        "Age": "40",
        "Income": "600000",
        "Graduate": "TRUE",
        "lifeCover": "7263750"
    },
    {
        "Age": "40",
        "Income": "700000",
        "Graduate": "TRUE",
        "lifeCover": "7000000"
    },
    {
        "Age": "40",
        "Income": "720000",
        "Graduate": "TRUE",
        "lifeCover": "7313572"
    },
    {
        "Age": "40",
        "Income": "750000",
        "Graduate": "TRUE",
        "lifeCover": "6500000"
    },
    {
        "Age": "40",
        "Income": "800000",
        "Graduate": "TRUE",
        "lifeCover": "9125000"
    },
    {
        "Age": "40",
        "Income": "805000",
        "Graduate": "TRUE",
        "lifeCover": "8900000"
    },
    {
        "Age": "40",
        "Income": "1000000",
        "Graduate": "TRUE",
        "lifeCover": "9758482.5"
    },
    {
        "Age": "40",
        "Income": "1200000",
        "Graduate": "TRUE",
        "lifeCover": "8354605.333"
    },
    {
        "Age": "40",
        "Income": "1300000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "40",
        "Income": "1400000",
        "Graduate": "TRUE",
        "lifeCover": "7500000"
    },
    {
        "Age": "40",
        "Income": "1500000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "40",
        "Income": "1600000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "40",
        "Income": "1700000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "40",
        "Income": "2000000",
        "Graduate": "TRUE",
        "lifeCover": "7896066"
    },
    {
        "Age": "40",
        "Income": "2400000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "40",
        "Income": "2500000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "40",
        "Income": "3600000",
        "Graduate": "TRUE",
        "lifeCover": "8202000"
    },
    {
        "Age": "40",
        "Income": "4000000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "40",
        "Income": "4500000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "40",
        "Income": "5500000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "41",
        "Income": "500000",
        "Graduate": "TRUE",
        "lifeCover": "5309580"
    },
    {
        "Age": "41",
        "Income": "520000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "41",
        "Income": "540000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "41",
        "Income": "550000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "41",
        "Income": "575000",
        "Graduate": "TRUE",
        "lifeCover": "7500000"
    },
    {
        "Age": "41",
        "Income": "600000",
        "Graduate": "TRUE",
        "lifeCover": "5802737.143"
    },
    {
        "Age": "41",
        "Income": "608950",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "41",
        "Income": "650000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "41",
        "Income": "700000",
        "Graduate": "TRUE",
        "lifeCover": "6250000"
    },
    {
        "Age": "41",
        "Income": "795000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "41",
        "Income": "798000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "41",
        "Income": "850000",
        "Graduate": "TRUE",
        "lifeCover": "7013850"
    },
    {
        "Age": "41",
        "Income": "890000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "41",
        "Income": "900000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "41",
        "Income": "950000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "41",
        "Income": "1000000",
        "Graduate": "TRUE",
        "lifeCover": "7146000"
    },
    {
        "Age": "41",
        "Income": "1200000",
        "Graduate": "TRUE",
        "lifeCover": "7500000"
    },
    {
        "Age": "41",
        "Income": "1300000",
        "Graduate": "TRUE",
        "lifeCover": "6800000"
    },
    {
        "Age": "41",
        "Income": "1400000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "41",
        "Income": "1500000",
        "Graduate": "TRUE",
        "lifeCover": "8180750"
    },
    {
        "Age": "41",
        "Income": "1850000",
        "Graduate": "TRUE",
        "lifeCover": "7500000"
    },
    {
        "Age": "41",
        "Income": "2000000",
        "Graduate": "TRUE",
        "lifeCover": "7500000"
    },
    {
        "Age": "41",
        "Income": "2200000",
        "Graduate": "TRUE",
        "lifeCover": "7166666.667"
    },
    {
        "Age": "41",
        "Income": "2400000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "41",
        "Income": "2500000",
        "Graduate": "TRUE",
        "lifeCover": "6400000"
    },
    {
        "Age": "41",
        "Income": "3000000",
        "Graduate": "TRUE",
        "lifeCover": "7500000"
    },
    {
        "Age": "41",
        "Income": "3600000",
        "Graduate": "TRUE",
        "lifeCover": "6000000"
    },
    {
        "Age": "42",
        "Income": "500000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "42",
        "Income": "505000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "42",
        "Income": "510000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "42",
        "Income": "550000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "42",
        "Income": "600000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "42",
        "Income": "650000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "42",
        "Income": "700000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "42",
        "Income": "840000",
        "Graduate": "TRUE",
        "lifeCover": "6160000"
    },
    {
        "Age": "42",
        "Income": "960000",
        "Graduate": "TRUE",
        "lifeCover": "7500000"
    },
    {
        "Age": "42",
        "Income": "1000000",
        "Graduate": "TRUE",
        "lifeCover": "5301336"
    },
    {
        "Age": "42",
        "Income": "1250000",
        "Graduate": "TRUE",
        "lifeCover": "7500000"
    },
    {
        "Age": "42",
        "Income": "1800000",
        "Graduate": "TRUE",
        "lifeCover": "7500000"
    },
    {
        "Age": "42",
        "Income": "2200000",
        "Graduate": "TRUE",
        "lifeCover": "6521267"
    },
    {
        "Age": "42",
        "Income": "2500000",
        "Graduate": "TRUE",
        "lifeCover": "5301336"
    },
    {
        "Age": "43",
        "Income": "500000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "43",
        "Income": "550000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "43",
        "Income": "600000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "43",
        "Income": "610000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "43",
        "Income": "611760",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "43",
        "Income": "720000",
        "Graduate": "TRUE",
        "lifeCover": "6340202"
    },
    {
        "Age": "43",
        "Income": "800000",
        "Graduate": "TRUE",
        "lifeCover": "6750000"
    },
    {
        "Age": "43",
        "Income": "900000",
        "Graduate": "TRUE",
        "lifeCover": "6862500"
    },
    {
        "Age": "43",
        "Income": "1000000",
        "Graduate": "TRUE",
        "lifeCover": "7500000"
    },
    {
        "Age": "43",
        "Income": "1010000",
        "Graduate": "TRUE",
        "lifeCover": "7500000"
    },
    {
        "Age": "43",
        "Income": "1100000",
        "Graduate": "TRUE",
        "lifeCover": "7500000"
    },
    {
        "Age": "43",
        "Income": "1200000",
        "Graduate": "TRUE",
        "lifeCover": "7329002.5"
    },
    {
        "Age": "43",
        "Income": "2000000",
        "Graduate": "TRUE",
        "lifeCover": "6723529.333"
    },
    {
        "Age": "43",
        "Income": "2500000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "43",
        "Income": "2800000",
        "Graduate": "TRUE",
        "lifeCover": "7500000"
    },
    {
        "Age": "43",
        "Income": "4000000",
        "Graduate": "TRUE",
        "lifeCover": "6500000"
    },
    {
        "Age": "44",
        "Income": "350000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "44",
        "Income": "500000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "44",
        "Income": "600000",
        "Graduate": "TRUE",
        "lifeCover": "5500000"
    },
    {
        "Age": "44",
        "Income": "700000",
        "Graduate": "TRUE",
        "lifeCover": "6250000"
    },
    {
        "Age": "44",
        "Income": "720000",
        "Graduate": "TRUE",
        "lifeCover": "5280000"
    },
    {
        "Age": "44",
        "Income": "800000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "44",
        "Income": "845000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "44",
        "Income": "900000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "44",
        "Income": "936000",
        "Graduate": "TRUE",
        "lifeCover": "7500000"
    },
    {
        "Age": "44",
        "Income": "1000000",
        "Graduate": "TRUE",
        "lifeCover": "6425000"
    },
    {
        "Age": "44",
        "Income": "1400000",
        "Graduate": "TRUE",
        "lifeCover": "7000000"
    },
    {
        "Age": "44",
        "Income": "1500000",
        "Graduate": "TRUE",
        "lifeCover": "7500000"
    },
    {
        "Age": "44",
        "Income": "3000000",
        "Graduate": "TRUE",
        "lifeCover": "7500000"
    },
    {
        "Age": "44",
        "Income": "5000000",
        "Graduate": "TRUE",
        "lifeCover": "7500000"
    },
    {
        "Age": "45",
        "Income": "500000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "45",
        "Income": "550000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "45",
        "Income": "600000",
        "Graduate": "TRUE",
        "lifeCover": "6035832"
    },
    {
        "Age": "45",
        "Income": "700000",
        "Graduate": "TRUE",
        "lifeCover": "7290000"
    },
    {
        "Age": "45",
        "Income": "800000",
        "Graduate": "TRUE",
        "lifeCover": "6666666.667"
    },
    {
        "Age": "45",
        "Income": "1000000",
        "Graduate": "TRUE",
        "lifeCover": "7020116.75"
    },
    {
        "Age": "45",
        "Income": "1100000",
        "Graduate": "TRUE",
        "lifeCover": "7500000"
    },
    {
        "Age": "45",
        "Income": "1200000",
        "Graduate": "TRUE",
        "lifeCover": "7500000"
    },
    {
        "Age": "45",
        "Income": "1291613",
        "Graduate": "TRUE",
        "lifeCover": "7500000"
    },
    {
        "Age": "45",
        "Income": "2000000",
        "Graduate": "TRUE",
        "lifeCover": "7500000"
    },
    {
        "Age": "45",
        "Income": "2200000",
        "Graduate": "TRUE",
        "lifeCover": "7500000"
    },
    {
        "Age": "45",
        "Income": "2500000",
        "Graduate": "TRUE",
        "lifeCover": "7500000"
    },
    {
        "Age": "45",
        "Income": "2600000",
        "Graduate": "TRUE",
        "lifeCover": "7500000"
    },
    {
        "Age": "45",
        "Income": "4000000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "45",
        "Income": "5000000",
        "Graduate": "TRUE",
        "lifeCover": "7500000"
    },
    {
        "Age": "46",
        "Income": "400000",
        "Graduate": "TRUE",
        "lifeCover": "9611184"
    },
    {
        "Age": "46",
        "Income": "500000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "46",
        "Income": "550000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "46",
        "Income": "600000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "46",
        "Income": "700000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "46",
        "Income": "750000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "46",
        "Income": "800000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "46",
        "Income": "1000000",
        "Graduate": "TRUE",
        "lifeCover": "5766813"
    },
    {
        "Age": "46",
        "Income": "1100000",
        "Graduate": "TRUE",
        "lifeCover": "6500000"
    },
    {
        "Age": "46",
        "Income": "1200000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "46",
        "Income": "1300000",
        "Graduate": "TRUE",
        "lifeCover": "6500000"
    },
    {
        "Age": "46",
        "Income": "5000000",
        "Graduate": "TRUE",
        "lifeCover": "7500000"
    },
    {
        "Age": "47",
        "Income": "500000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "47",
        "Income": "550000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "47",
        "Income": "600000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "47",
        "Income": "2800786",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "48",
        "Income": "500000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "48",
        "Income": "600000",
        "Graduate": "TRUE",
        "lifeCover": "6508286.667"
    },
    {
        "Age": "48",
        "Income": "650000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "48",
        "Income": "800000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "48",
        "Income": "1100000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "48",
        "Income": "1200000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "48",
        "Income": "1500000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "48",
        "Income": "5000000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "49",
        "Income": "1000000",
        "Graduate": "TRUE",
        "lifeCover": "7500000"
    },
    {
        "Age": "49",
        "Income": "1200000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "49",
        "Income": "2500000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "49",
        "Income": "7500000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "50",
        "Income": "800000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "50",
        "Income": "880000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "50",
        "Income": "894510",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "50",
        "Income": "1100000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "50",
        "Income": "2500000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "50",
        "Income": "2749500",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "50",
        "Income": "4000000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "50",
        "Income": "90000000",
        "Graduate": "TRUE",
        "lifeCover": "10000000"
    },
    {
        "Age": "51",
        "Income": "500000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "51",
        "Income": "800000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "51",
        "Income": "1400000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "52",
        "Income": "500000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "52",
        "Income": "550000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "52",
        "Income": "600000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "52",
        "Income": "1500000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "52",
        "Income": "1800000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "52",
        "Income": "2500000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "53",
        "Income": "500000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "53",
        "Income": "600000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "53",
        "Income": "700000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "53",
        "Income": "1500000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "54",
        "Income": "1000000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "55",
        "Income": "575000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "55",
        "Income": "600000",
        "Graduate": "FALSE",
        "lifeCover": "5000000"
    },
    {
        "Age": "55",
        "Income": "600000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "55",
        "Income": "700000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "55",
        "Income": "800000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "55",
        "Income": "840000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "55",
        "Income": "900000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "55",
        "Income": "1000000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "55",
        "Income": "1500000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "55",
        "Income": "3600000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "55",
        "Income": "5000000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    },
    {
        "Age": "55",
        "Income": "6400000",
        "Graduate": "TRUE",
        "lifeCover": "5000000"
    }
]
