import { Container } from 'react-bootstrap';
import { Route, Redirect, Router } from 'react-router-dom';
import Dashboard from './pages/dashboard';
import FinancialImmunity from './pages/financialImmunity';
import FinancialScore from './pages/financialScore';
import PartnerComponent from './pages/partner';

import './css/dashboard.css';
import './App.css';

export const history = require('history').createBrowserHistory({
  basename: '/',
});

function App() {
  return (
    <Router history={history}>
      <Route exact path='/'>
        <Dashboard />
      </Route>{' '}
      <Route exact path='/financial-immunity'>
        {/* <Container> */}
        <FinancialScore />
        {/* </Container> */}
      </Route>{' '}
      <Route exact path='/human-life-cover'>
        <div className='life-cover'>
          <Container>
            <FinancialImmunity />
          </Container>
        </div>
      </Route>
      <Route exact path='/partner-api'>
        <PartnerComponent />
      </Route>
    </Router>
  );
}

export default App;
